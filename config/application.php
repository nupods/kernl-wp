<?php
/**
 * Your base production configuration goes in this file. Environment-specific
 * overrides go in their respective config/environments/{{WP_ENV}}.php file.
 *
 * A good default policy is to deviate from the production config as little as
 * possible. Try to define as much of your configuration in this file as you
 * can.
 */

use Roots\WPConfig\Config;

/** @var string Directory containing all of the site's files */
$root_dir = dirname(__DIR__);

/** @var string Document Root */
$webroot_dir = $root_dir . '/web';

/**
 * Expose global env() function from oscarotero/env
 */
Env::init();

/**
 * Use Dotenv to set required environment variables and load .env file in root
 */
$dotenv = Dotenv\Dotenv::create($root_dir);
if (file_exists($root_dir . '/.env')) {
    $dotenv->load();
    $dotenv->required(['WP_HOME', 'WP_SITEURL']);
    if (!env('DATABASE_URL')) {
        $dotenv->required(['DB_NAME', 'DB_USER', 'DB_PASSWORD']);
    }
}

/**
 * Set up our global environment constant and load its config first
 * Default: production
 */
define('WP_ENV', env('WP_ENV') ?: 'production');
define('WP_HOST', env('WP_HOST') ?: false);

/**
 * URLs
 */
Config::define('WP_HOME', env('WP_HOME'));
Config::define('WP_SITEURL', env('WP_SITEURL'));

/**
 * Custom Content Directory
 */
Config::define('CONTENT_DIR', '/app');
Config::define('WP_CONTENT_DIR', $webroot_dir . Config::get('CONTENT_DIR'));
Config::define('WP_CONTENT_URL', Config::get('WP_HOME') . Config::get('CONTENT_DIR'));

/**
 * DB settings
 */
Config::define('DB_NAME', env('DB_NAME'));
Config::define('DB_USER', env('DB_USER'));
Config::define('DB_PASSWORD', env('DB_PASSWORD'));
Config::define('DB_HOST', env('DB_HOST') ?: 'localhost');
Config::define('DB_CHARSET', 'utf8mb4');
Config::define('DB_COLLATE', '');
$table_prefix = env('DB_PREFIX') ?: 'wp_';

if (env('DATABASE_URL')) {
    $dsn = (object) parse_url(env('DATABASE_URL'));

    Config::define('DB_NAME', substr($dsn->path, 1));
    Config::define('DB_USER', $dsn->user);
    Config::define('DB_PASSWORD', isset($dsn->pass) ? $dsn->pass : null);
    Config::define('DB_HOST', isset($dsn->port) ? "{$dsn->host}:{$dsn->port}" : $dsn->host);
}

/**
 * Authentication Unique Keys and Salts
 */
Config::define('AUTH_KEY', env('AUTH_KEY'));
Config::define('SECURE_AUTH_KEY', env('SECURE_AUTH_KEY'));
Config::define('LOGGED_IN_KEY', env('LOGGED_IN_KEY'));
Config::define('NONCE_KEY', env('NONCE_KEY'));
Config::define('AUTH_SALT', env('AUTH_SALT'));
Config::define('SECURE_AUTH_SALT', env('SECURE_AUTH_SALT'));
Config::define('LOGGED_IN_SALT', env('LOGGED_IN_SALT'));
Config::define('NONCE_SALT', env('NONCE_SALT'));

/**
 * Custom Settings
 */
Config::define('AUTOMATIC_UPDATER_DISABLED', true);
Config::define('DISABLE_WP_CRON', env('DISABLE_WP_CRON') ?: false);
// Disable the plugin and theme file editor in the admin
Config::define('DISALLOW_FILE_EDIT', true);
// Disable plugin and theme updates and installation from the admin
Config::define('DISALLOW_FILE_MODS', true);

// Revisions
Config::define('WP_POST_REVISIONS', getenv('WP_POST_REVISIONS') ?: 0);

// WP DB Migrate Pro
Config::define('WPMDB_LICENCE', env('PLUGIN_WPMDB_KEY'));
Config::define('WPMDB_WP_FILESYSTEM', false);

// Condition block for non-local environements
if (env('WP_HOST') !== 'valet') {
    // NU proxy (except on local)
    Config::define('WP_PROXY_HOST', 'www.proxy.neu.edu');
    Config::define('WP_PROXY_PORT', '3128');
    Config::define('WP_USEPROXY', true);

    // set HTTPS server variable
    $_SERVER['HTTPS'] = 'on';
}

// Multisite
Config::define('WP_ALLOW_MULTISITE', env('WP_ALLOW_MULTISITE') ?: false);
Config::define('MULTISITE', env('MULTISITE') ?: false);
Config::define('SUBDOMAIN_INSTALL', env('SUBDOMAIN_INSTALL') ?: false);
Config::define('DOMAIN_CURRENT_SITE', env('DOMAIN_CURRENT_SITE'));
Config::define('PATH_CURRENT_SITE', env('PATH_CURRENT_SITE') ?: '/');
Config::define('SITE_ID_CURRENT_SITE', env('SITE_ID_CURRENT_SITE') ?: 1);
Config::define('BLOG_ID_CURRENT_SITE', env('BLOG_ID_CURRENT_SITE') ?: 1);

// Sibboleth config
Config::define('SHIBBOLETH_ATTRIBUTE_ACCESS_METHOD', 'standard');
Config::define('SHIBBOLETH_LOGIN_URL', 'https://'.env('DOMAIN_CURRENT_SITE').'/Shibboleth.sso/Login');
Config::define('SHIBBOLETH_LOGOUT_URL', 'https://'.env('DOMAIN_CURRENT_SITE').'/Shibboleth.sso/Logout');
Config::define('SHIBBOLETH_PASSWORD_CHANGE_URL', 'http://my.northeastern.edu/myneu/accounts/how.html');
Config::define('SHIBBOLETH_PASSWORD_RESET_URL', 'http://my.northeastern.edu/myneu/accounts/how.html');
Config::define('SHIBBOLETH_SPOOF_KEY', 'abcdefghijklmnopqrstuvwxyz');
Config::define('SHIBBOLETH_DEFAULT_TO_SHIB_LOGIN', false);
Config::define('SHIBBOLETH_AUTO_LOGIN', true);
Config::define('SHIBBOLETH_BUTTON_TEXT', 'Login with myNortheastern account');
Config::define('SHIBBOLETH_DISABLE_LOCAL_AUTH', false);
Config::define('SHIBBOLETH_HEADERS', [
    'username' => [
        'name' => 'eppn'
    ],
    'first_name' => [
        'name' => 'givenName',
        'managed' => 'on'
    ],
    'last_name' => [
        'name' => 'sn',
        'managed' => 'on'
    ],
    'nickname' => [
        'name' => 'eppn',
        'managed' => 'off'
    ],
    'display_name' => [
        'name' => 'cn',
        'managed' => 'off'
    ],
    'email' => [
        'name' => 'mail',
        'managed' => 'on'
    ]
]);
Config::define('SHIBBOLETH_CREATE_ACCOUNTS', true);
Config::define('SHIBBOLETH_AUTO_COMBINE_ACCOUNTS', 'disallow');
Config::define('SHIBBOLETH_MANUALLY_COMBINE_ACCOUNTS', 'allow');
Config::define('SHIBBOLETH_ROLES', [
    'administrator' => [
        'header' => 'entitlement',
        'value' => 'urn:mace:example.edu:entitlement:wordpress:admin'
    ],
    'author' => [
        'header' => 'affiliation',
        'value' => 'faculty'
    ]
]);
Config::define('SHIBBOLETH_DEFAULT_ROLE', 'subscriber');
Config::define('SHIBBOLETH_UPDATE_ROLES', false);
Config::define('SHIBBOLETH_LOGGING', ['account_merge', 'account_create', 'auth', 'role_update']);
Config::define('SHIBBOLETH_DISALLOW_FILE_MODS', true);

/**
 * Debugging Settings
 */
Config::define('WP_DEBUG_DISPLAY', false);
Config::define('SCRIPT_DEBUG', false);
ini_set('display_errors', 0);

/**
 * Environment configs
 */

$env_config = __DIR__ . '/environments/' . WP_ENV . '.php';

if (file_exists($env_config)) {
    require_once $env_config;
}

Config::apply();

/**
 * Bootstrap WordPress
 */
if (!defined('ABSPATH')) {
    define('ABSPATH', $webroot_dir . '/wp/');
}
